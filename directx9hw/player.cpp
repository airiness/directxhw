//=============================================================================
//
// モデル処理 [player.cpp]
// Author : 
//
//=============================================================================
#include "main.h"
#include "input.h"
#include "player.h"
#include "camera.h"
#include "shadow.h"

//*****************************************************************************
// マクロ定義
//*****************************************************************************
#define	MODEL_PLAYER		"data/MODEL/airplane000.x"		// 読み込むモデル名

#define	VALUE_MOVE			(5.0f)							// 移動量
#define	VALUE_ROTATE		(D3DX_PI * 0.02f)				// 回転量

#define PLAYER_SHADOW_SIZE	(25.0f)							// 影の大きさ


//*****************************************************************************
// プロトタイプ宣言
//*****************************************************************************


//*****************************************************************************
// グローバル変数
//*****************************************************************************

static PLAYER				g_Player;						// プレイヤー


//=============================================================================
// 初期化処理
//=============================================================================
HRESULT InitPlayer(void)
{
	LPDIRECT3DDEVICE9 pDevice = GetDevice();

								// 位置・回転・スケールの初期設定
	g_Player.pos = D3DXVECTOR3(0.0f, 20.0f, 0.0f);
	g_Player.rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	g_Player.scl = D3DXVECTOR3(1.0f, 1.0f, 1.0f);

	g_Player.spd = 0.0f;		// 移動スピードクリア

	g_Player.pD3DTexture = NULL;
	g_Player.pD3DXMesh = NULL;
	g_Player.pD3DXBuffMat = NULL;
	g_Player.nNumMat = 0;

	// Xファイルの読み込み
	if(FAILED(D3DXLoadMeshFromX(MODEL_PLAYER,			// 読み込むモデルファイル名(Xファイル)
								D3DXMESH_SYSTEMMEM,		// メッシュの作成オプションを指定
								pDevice,				// IDirect3DDevice9インターフェイスへのポインタ
								NULL,					// 隣接性データを含むバッファへのポインタ
								&g_Player.pD3DXBuffMat,	// マテリアルデータを含むバッファへのポインタ
								NULL,					// エフェクトインスタンスの配列を含むバッファへのポインタ
								&g_Player.nNumMat,		// D3DXMATERIAL構造体の数
								&g_Player.pD3DXMesh)))	// ID3DXMeshインターフェイスへのポインタのアドレス
	{
		return E_FAIL;
	}

#if 0
	// テクスチャの読み込み
	D3DXCreateTextureFromFile(pDevice,					// デバイスへのポインタ
		TEXTURE_FILENAME,		// ファイルの名前
		&g_Player.pD3DTexture);	// 読み込むメモリー
#endif

	// 影の初期化
	D3DXVECTOR3 pos = g_Player.pos;
	pos.y = 0.1f;
	g_Player.shadowIdx = CreateShadow(pos, g_Player.scl);

	return S_OK;
}

void UninitPlayer(void)
{
	if(g_Player.pD3DTexture != NULL)
	{
		g_Player.pD3DTexture->Release();
		g_Player.pD3DTexture = NULL;
	}

	if(g_Player.pD3DXMesh != NULL)
	{
		g_Player.pD3DXMesh->Release();
		g_Player.pD3DXMesh = NULL;
	}

	if(g_Player.pD3DXBuffMat != NULL)
	{
		g_Player.pD3DXBuffMat->Release();
		g_Player.pD3DXBuffMat = NULL;
	}
}


void UpdatePlayer(void)
{
	CAMERA *cam = GetCamera();


	int		dir = 0;	

	if (GetKeyboardPress(DIK_LEFT))
	{
		dir |= 8;
		g_Player.spd = VALUE_MOVE;
	}
	if (GetKeyboardPress(DIK_RIGHT))
	{
		dir |= 4;
		g_Player.spd = VALUE_MOVE;
	}
	if (GetKeyboardPress(DIK_UP))
	{
		dir |= 2;
		g_Player.spd = VALUE_MOVE;
	}
	if (GetKeyboardPress(DIK_DOWN))
	{
		dir |= 1;
		g_Player.spd = VALUE_MOVE;
	}

	if (GetKeyboardPress(DIK_SPACE))
	{

		// shoot  a bullet

		//g_Player.pos.z = g_Player.pos.x = 0.0f;
		//dir = 1;
		//g_Player.spd = 0.0f;
	}

	// 入力されたキーに合わせて向きを決める
	float roty = 0.0f;
	switch (dir)
	{
	case 1:	// 下向き
		roty = 0.0f;
		break;

	case 2:	// 上向き
		roty = D3DX_PI;
		break;

	case 4:	// 右向き
		roty = -D3DX_PI / 2;
		break;

	case 8:	// 左向き
		roty = D3DX_PI / 2;
		break;

	case 5:	// 右下向き
		roty = -D3DX_PI / 4;
		break;

	case 6:	// 右上向き
		roty = -D3DX_PI / 4 * 3;
		break;

	case 9:	// 左下向き
		roty = D3DX_PI / 4;
		break;

	case 10: // 左上向き
		roty = D3DX_PI / 4 * 3;
		break;

	case 0:
		roty = g_Player.rot.y - cam->rot.y;
		break;

	}

	// Key入力があったら移動処理する
	//if (dir > 0)
	{
		// カメラに対して入力のあった方向へプレイヤーを向かせて移動させる
		g_Player.rot.y = roty + cam->rot.y;
		g_Player.pos.x -= sinf(g_Player.rot.y) * g_Player.spd;
		g_Player.pos.z -= cosf(g_Player.rot.y) * g_Player.spd;

		// カメラの注視点と視点を主人公に追従させる
		if (GetIPCnt() == 0)
		{
			cam->at.x = g_Player.pos.x;
			cam->at.z = g_Player.pos.z;
			cam->pos.x = cam->at.x - sinf(cam->rot.y) * cam->len;
			cam->pos.z = cam->at.z - cosf(cam->rot.y) * cam->len;
		}
	}

	// Keyを話した時に少し慣性っぽくする
	g_Player.spd *= 0.9f;

	g_Player.pos.x = (std::min)(g_Player.pos.x, 310.0f);
	g_Player.pos.x = (std::max)(g_Player.pos.x, -310.0f);
	g_Player.pos.z = (std::min)(g_Player.pos.z, 310.0f);
	g_Player.pos.z = (std::max)(g_Player.pos.z, -310.0f);


	// 影もプレイヤーの位置に合わせる
	D3DXVECTOR3 pos = g_Player.pos;
	pos.y = 0.1f;
	SetPositionShadow(g_Player.shadowIdx, pos, g_Player.scl);

}

//=============================================================================
// 描画処理
//=============================================================================
void DrawPlayer(void)
{
	LPDIRECT3DDEVICE9 pDevice = GetDevice();
	D3DXMATRIX mtxScl, mtxRot, mtxTranslate;
	D3DXMATERIAL *pD3DXMat;
	D3DMATERIAL9 matDef;

	// ワールドマトリックスの初期化
	D3DXMatrixIdentity(&g_Player.mtxWorld);

	// スケールを反映
	D3DXMatrixScaling(&mtxScl, g_Player.scl.x, g_Player.scl.y, g_Player.scl.z);
	D3DXMatrixMultiply(&g_Player.mtxWorld, &g_Player.mtxWorld, &mtxScl);

	// 回転を反映
	D3DXMatrixRotationYawPitchRoll(&mtxRot, g_Player.rot.y, g_Player.rot.x, g_Player.rot.z);
	D3DXMatrixMultiply(&g_Player.mtxWorld, &g_Player.mtxWorld, &mtxRot);

	// 移動を反映
	D3DXMatrixTranslation(&mtxTranslate, g_Player.pos.x, g_Player.pos.y, g_Player.pos.z);
	D3DXMatrixMultiply(&g_Player.mtxWorld, &g_Player.mtxWorld, &mtxTranslate);

	// ワールドマトリックスの設定
	pDevice->SetTransform(D3DTS_WORLD, &g_Player.mtxWorld);

	// 現在のマテリアルを取得
	pDevice->GetMaterial(&matDef);

	// マテリアル情報に対するポインタを取得
	pD3DXMat = (D3DXMATERIAL*)g_Player.pD3DXBuffMat->GetBufferPointer();

	for(int nCntMat = 0; nCntMat < (int)g_Player.nNumMat; nCntMat++)
	{
		// マテリアルの設定
		pDevice->SetMaterial(&pD3DXMat[nCntMat].MatD3D);

		// テクスチャの設定
		pDevice->SetTexture(0, g_Player.pD3DTexture);

		// 描画
		g_Player.pD3DXMesh->DrawSubset(nCntMat);
	}

	// マテリアルをデフォルトに戻す
	pDevice->SetMaterial(&matDef);
}


//=============================================================================
// プレイヤー情報を取得
//=============================================================================
PLAYER *GetPlayer(void)
{
	return &g_Player;
}

