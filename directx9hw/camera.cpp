#include "main.h"
#include "input.h"
#include "camera.h"

#define	POS_X_CAM		(0.0f)			// カメラの初期位置(X座標)
#define	POS_Y_CAM		(100.0f)			// カメラの初期位置(Y座標)
#define	POS_Z_CAM		(-150.0f)		// カメラの初期位置(Z座標)

#define	VIEW_ANGLE		(D3DXToRadian(45.0f))							// ビュー平面の視野角
#define	VIEW_ASPECT		((float)SCREEN_WIDTH / (float)SCREEN_HEIGHT)	// ビュー平面のアスペクト比	
#define	VIEW_NEAR_Z		(10.0f)											// ビュー平面のNearZ値
#define	VIEW_FAR_Z		(10000.0f)										// ビュー平面のFarZ値

#define	VALUE_MOVE_CAMERA	(2.0f)										// カメラの移動量
#define	VALUE_ROTATE_CAMERA	(D3DX_PI * 0.01f)							// カメラの回転量

#define	CAM_IP_MAX			(2)

//*****************************************************************************
// グローバル変数
//*****************************************************************************
CAMERA			g_Camera;				// カメラデータ

int				g_ip_cnt = 0;			// 補間処理個数
INTERPOLATION	g_ip[CAM_IP_MAX];		// 補間用ワーク (at/eye)

D3DXVECTOR3 move_tbl_at[] = {
	D3DXVECTOR3(0.0f, 0.0f, 0.0f),
	D3DXVECTOR3(0.0f, 0.0f, 0.0f),
	D3DXVECTOR3(100.0f, 0.0f, 0.0f),
	D3DXVECTOR3(100.0f, 0.0f, -100.0f),
	D3DXVECTOR3(0.0f, 0.0f, 0.0f),
};

D3DXVECTOR3 move_tbl_eye[] = {
	D3DXVECTOR3(0.0f, 50.0f, -100.0f),
	D3DXVECTOR3(0.0f, 50.0f, -10),
	D3DXVECTOR3(100.0f, 50.0f, -100.0f),
	D3DXVECTOR3(100.0f, 100.0f, -200.0f),
	D3DXVECTOR3(0.0f, 50.0f, -100.0f),
};


//=============================================================================
// 初期化処理
//=============================================================================
void InitCamera(void)
{
	g_Camera.pos = D3DXVECTOR3(POS_X_CAM, POS_Y_CAM, POS_Z_CAM);
	g_Camera.at = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	g_Camera.up = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	g_Camera.rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	// 視点と注視点の距離を計算
	float vx, vz;
	vx = g_Camera.pos.x - g_Camera.at.x;
	vz = g_Camera.pos.z - g_Camera.at.z;
	g_Camera.len = sqrtf(vx * vx + vz * vz);

}


//=============================================================================
// カメラの終了処理
//=============================================================================
void UninitCamera(void)
{
	g_ip_cnt = 0;

}


//=============================================================================
// カメラの更新処理
//=============================================================================
void UpdateCamera(void)
{
	if (g_ip_cnt <= 0)
	{
		if (GetKeyboardPress(DIK_A))
		{
			if (GetKeyboardPress(DIK_W))
			{// 左前移動
				g_Camera.pos.x -= cosf(g_Camera.rot.y + D3DX_PI * 0.25f) * VALUE_MOVE_CAMERA;
				g_Camera.pos.z += sinf(g_Camera.rot.y + D3DX_PI * 0.25f) * VALUE_MOVE_CAMERA;
			}
			else if (GetKeyboardPress(DIK_S))
			{// 左後移動
				g_Camera.pos.x -= cosf(g_Camera.rot.y - D3DX_PI * 0.25f) * VALUE_MOVE_CAMERA;
				g_Camera.pos.z += sinf(g_Camera.rot.y - D3DX_PI * 0.25f) * VALUE_MOVE_CAMERA;
			}
			else
			{// 左移動
				g_Camera.pos.x -= cosf(g_Camera.rot.y) * VALUE_MOVE_CAMERA;
				g_Camera.pos.z += sinf(g_Camera.rot.y) * VALUE_MOVE_CAMERA;
			}

			g_Camera.at.x = g_Camera.pos.x + sinf(g_Camera.rot.y) * g_Camera.len;
			g_Camera.at.z = g_Camera.pos.z + cosf(g_Camera.rot.y) * g_Camera.len;
		}
		else if (GetKeyboardPress(DIK_D))
		{
			if (GetKeyboardPress(DIK_W))
			{// 右前移動
				g_Camera.pos.x += cosf(g_Camera.rot.y - D3DX_PI * 0.25f) * VALUE_MOVE_CAMERA;
				g_Camera.pos.z -= sinf(g_Camera.rot.y - D3DX_PI * 0.25f) * VALUE_MOVE_CAMERA;
			}
			else if (GetKeyboardPress(DIK_S))
			{// 右後移動
				g_Camera.pos.x += cosf(g_Camera.rot.y + D3DX_PI * 0.25f) * VALUE_MOVE_CAMERA;
				g_Camera.pos.z -= sinf(g_Camera.rot.y + D3DX_PI * 0.25f) * VALUE_MOVE_CAMERA;
			}
			else
			{// 右移動
				g_Camera.pos.x += cosf(g_Camera.rot.y) * VALUE_MOVE_CAMERA;
				g_Camera.pos.z -= sinf(g_Camera.rot.y) * VALUE_MOVE_CAMERA;
			}

			g_Camera.at.x = g_Camera.pos.x + sinf(g_Camera.rot.y) * g_Camera.len;
			g_Camera.at.z = g_Camera.pos.z + cosf(g_Camera.rot.y) * g_Camera.len;
		}
		else if (GetKeyboardPress(DIK_W))
		{// 前移動
			g_Camera.pos.x += sinf(g_Camera.rot.y) * VALUE_MOVE_CAMERA;
			g_Camera.pos.z += cosf(g_Camera.rot.y) * VALUE_MOVE_CAMERA;

			g_Camera.at.x = g_Camera.pos.x + sinf(g_Camera.rot.y) * g_Camera.len;
			g_Camera.at.z = g_Camera.pos.z + cosf(g_Camera.rot.y) * g_Camera.len;
		}
		else if (GetKeyboardPress(DIK_S))
		{// 後移動
			g_Camera.pos.x -= sinf(g_Camera.rot.y) * VALUE_MOVE_CAMERA;
			g_Camera.pos.z -= cosf(g_Camera.rot.y) * VALUE_MOVE_CAMERA;

			g_Camera.at.x = g_Camera.pos.x + sinf(g_Camera.rot.y) * g_Camera.len;
			g_Camera.at.z = g_Camera.pos.z + cosf(g_Camera.rot.y) * g_Camera.len;
		}

		if (GetKeyboardPress(DIK_Z))
		{// 視点旋回「左」
			g_Camera.rot.y += VALUE_ROTATE_CAMERA;
			if (g_Camera.rot.y > D3DX_PI)
			{
				g_Camera.rot.y -= D3DX_PI * 2.0f;
			}

			g_Camera.pos.x = g_Camera.at.x - sinf(g_Camera.rot.y) * g_Camera.len;
			g_Camera.pos.z = g_Camera.at.z - cosf(g_Camera.rot.y) * g_Camera.len;
		}

		if (GetKeyboardPress(DIK_C))
		{// 視点旋回「右」
			g_Camera.rot.y -= VALUE_ROTATE_CAMERA;
			if (g_Camera.rot.y < -D3DX_PI)
			{
				g_Camera.rot.y += D3DX_PI * 2.0f;
			}

			g_Camera.pos.x = g_Camera.at.x - sinf(g_Camera.rot.y) * g_Camera.len;
			g_Camera.pos.z = g_Camera.at.z - cosf(g_Camera.rot.y) * g_Camera.len;
		}

		if (GetKeyboardPress(DIK_Y))
		{// 視点移動「上」
			g_Camera.pos.y += VALUE_MOVE_CAMERA;
		}

		if (GetKeyboardPress(DIK_N))
		{// 視点移動「下」
			g_Camera.pos.y -= VALUE_MOVE_CAMERA;
		}

		if (GetKeyboardPress(DIK_Q))
		{// 注視点旋回「左」
			g_Camera.rot.y -= VALUE_ROTATE_CAMERA;
			if (g_Camera.rot.y < -D3DX_PI)
			{
				g_Camera.rot.y += D3DX_PI * 2.0f;
			}

			g_Camera.at.x = g_Camera.pos.x + sinf(g_Camera.rot.y) * g_Camera.len;
			g_Camera.at.z = g_Camera.pos.z + cosf(g_Camera.rot.y) * g_Camera.len;
		}

		if (GetKeyboardPress(DIK_E))
		{// 注視点旋回「右」
			g_Camera.rot.y += VALUE_ROTATE_CAMERA;
			if (g_Camera.rot.y > D3DX_PI)
			{
				g_Camera.rot.y -= D3DX_PI * 2.0f;
			}

			g_Camera.at.x = g_Camera.pos.x + sinf(g_Camera.rot.y) * g_Camera.len;
			g_Camera.at.z = g_Camera.pos.z + cosf(g_Camera.rot.y) * g_Camera.len;
		}

		if (GetKeyboardPress(DIK_T))
		{// 注視点移動「上」
			g_Camera.at.y += VALUE_MOVE_CAMERA;
		}

		if (GetKeyboardPress(DIK_B))
		{// 注視点移動「下」
			g_Camera.at.y -= VALUE_MOVE_CAMERA;
		}

		if (GetKeyboardPress(DIK_U))
		{// 近づく
			g_Camera.len -= VALUE_MOVE_CAMERA;
		}

		if (GetKeyboardPress(DIK_M))
		{// 離れる
			g_Camera.len += VALUE_MOVE_CAMERA;
		}

		// 自動カメラ処理開始？
		if (GetKeyboardTrigger(DIK_9))
		{
			memset(&g_ip[0], 0, sizeof(INTERPOLATION));
			memset(&g_ip[1], 0, sizeof(INTERPOLATION));

			g_ip[0].dt  = 0.01f;
			g_ip[0].tbl = move_tbl_at;
			g_ip[0].tbl_cnt = sizeof(move_tbl_at) / sizeof(D3DXVECTOR3);
			g_ip[0].use = true;
			g_ip_cnt++;

			g_ip[1].dt = 0.01f;
			g_ip[1].tbl = move_tbl_eye;
			g_ip[1].tbl_cnt = sizeof(move_tbl_eye) / sizeof(D3DXVECTOR3);
			g_ip[1].use = true;
			g_ip_cnt++;
		}


		// カメラを初期に戻す
		if (GetKeyboardTrigger(DIK_SPACE))
		{
			UninitCamera();
			InitCamera();
		}
	}
	else
	{	// 自動カメラ処理

		for (int i = 0; i < CAM_IP_MAX; i++)
		{
			// 使われているなら処理する
			if (g_ip[i].use == true)
			{
				// 移動処理
				g_ip[i].move_time += g_ip[i].dt;					// アニメーションの合計時間に足す

				int		index = (int)g_ip[i].move_time;
				float	time = g_ip[i].move_time - index;
				int		size = g_ip[i].tbl_cnt;

				if (index > (size - 2))	// ゴールをオーバーしていたら、ゴールへ戻す
				{
					g_ip[i].move_time = 0.0f;
					index = 0;
				}

				// X座標を求める	X = StartX + (EndX - StartX) * 今の時間
				D3DXVECTOR3 vec = g_ip[i].tbl[index + 1] - g_ip[i].tbl[index];

				if (i == 0)
				{
					g_Camera.at = g_ip[i].tbl[index] + vec * time;
				}
				else
				{
					g_Camera.pos = g_ip[i].tbl[index] + vec * time;
				}

			}
		}

		// カメラを初期に戻す
		if (GetKeyboardTrigger(DIK_SPACE))
		{
			UninitCamera();
			InitCamera();
		}

	}

}


//=============================================================================
// カメラの更新
//=============================================================================
void SetCamera(void) 
{
	LPDIRECT3DDEVICE9 pDevice = GetDevice();

	// ビューマトリックスの初期化
	D3DXMatrixIdentity(&g_Camera.mtxView);

	// ビューマトリックスの作成
	D3DXMatrixLookAtLH(&g_Camera.mtxView,
		&g_Camera.pos,		// カメラの視点(位置)
		&g_Camera.at,		// カメラの注視点
		&g_Camera.up);		// カメラの上方向ベクトル

	// ビューマトリックスの設定
	pDevice->SetTransform(D3DTS_VIEW, &g_Camera.mtxView);

	// プロジェクションマトリックスの初期化
	D3DXMatrixIdentity(&g_Camera.mtxProjection);

	// プロジェクションマトリックスの作成
	D3DXMatrixPerspectiveFovLH(&g_Camera.mtxProjection,
		VIEW_ANGLE,			// ビュー平面の視野角
		VIEW_ASPECT,		// ビュー平面のアスペクト比
		VIEW_NEAR_Z,		// ビュー平面のNearZ値
		VIEW_FAR_Z);		// ビュー平面のFarZ値

	// プロジェクションマトリックスの設定(透視変換の設定)
	pDevice->SetTransform(D3DTS_PROJECTION, &g_Camera.mtxProjection);
}


//=============================================================================
// カメラの取得
//=============================================================================
CAMERA *GetCamera(void) 
{
	return &g_Camera;
}


int GetIPCnt(void)
{
	return g_ip_cnt;
}


bool GetIPWork(void)
{
	return &g_ip[0];
}

